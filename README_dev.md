
# Manuel Développeur :

# Tableau de contenu : 

La description des arguments utilisés en paramètre sont présents dans le script avec des Keyword arguments :

#### Description précise du script detectEMG_compute_f_score

##### liste et redirection de chaque fonction :

* [load_data](#load_data-)
* [detect_one_trial_and_chan](#detect_one_trial_and_chan-)
* [detection_and_compute_f_score](#detection_and_compute_f_score-)
* [chan_in_dict](#chan_in_dict-)
* [get_list_chan](#get_list_chan-)
* [open_file_manu](#open_file_manu-)
* [get_manu](#get_manu-)
* [count_true_or_false_neg_pos](#count_true_or_false_neg_pos-)

#### Description précise du optimisation_script 

##### liste et redirection de chaque méthode :

* [__init__](#init-)
* [__getitem__](#getitem-)
* [__setitem__](#setitem-)
* [affiche_matrice](#affiche_matrice-) 
* [compute_f_score](#compute_f_score-)
* [matrice_computation](#matrix_computation-)
* [pivot_x et pivot_y](#pivot_x-et-pivot_y-)
* [search_high_value](#search_high_value-)
* [only_f_score](#only_f_score-)
* [try_f_optimize](#try_f_optimize-)

*** 

## Description précise du script detectEMG_compute_f_score :

Si votre dossier contenant les packages Events et evtTools sont localisés dans un endroit que le script présent. Changer le "sys.path.append(os.path.abspath("/home/calcul/trunk") par votre PATH où le fichier trunk est localisé.

Changer les chemins associés au fichier BDF et Manuel via les variables pathBDF et pathManu.

Mettre vos dictionnaires d'intérêt pour trig_id, eventType, correct_resp_id, emg_id.

### Description des fonctions :

#### load_data : 

Charge les fichiers brutes, les variables en global pour raw et lat_lag n'est pas à retirer sinon, les autres fonctions utilisant ces variables retournera une erreur.
Nom de fichier brut présent dans nameBDF -> changer le nom si vous voulez

Les variables retournés EpEvts, dataEpochs, epochTime, T0 sont utilisés dans la détections essai par essai, ainsi que dans la fonction detection_and_compute_f_score

* Keywords 

		* -- sujet
		* -- tmin
		* -- tmax
		* -- raw
		* -- lat_lag 

#### detect_one_trial_and_chan : 

Récupère les onsets et offsets provenant directement de la détection, cette fonction sera appelés dans la fonction detection_and_compte_f_score en récupérant les onsets directements 

* Keywords

		* -- c
		* -- current
		* -- epochTime
		* -- t0

#### detection_and_compute_f_score : 

En utilisant les seuils d'intérêts thRaw, thTk, ainsi que les paramètres d'intérêts supplémentaire (Keywords -> Paramètres d'intérêts)

Permet de faire la détection dans une boucle essai par essai via la détection de detect_one_trial_and_chan (première boucle en prenant la range jusqu'à la taille de dataEpochs en fin), et la deuxième boucle permettra de changer de canal entre 0 et 1 ici

une fois les onsets détectés automatiquement mis dans onset_detect_auto, on fait la comparaison entre manu et onset_detect_auto, manu correspond au dictionnaire de liste d'objet Event mis en place dans get_manu. 

Comparaison faite avec count_true_or_false_neg_pos.

La fonction fera le calcul du F score en faisant la somme avant du nombre de TP (Vrai positive) , nb_FN (Faux négative) et du nb_FP (faux positive) présent dans TP, FN, FP

Va retourner l'inverse du F_score via la variable invF_measure

* Keywords 

		* -- EpEvts
		* -- dataEpoch 
		* -- epochTime
		* -- manu
		* -- thRaw
		* -- thTk
		* -- time_limitRaw
		* -- time_limitTkeo
		* -- min_samplesRaw
		* -- min_sampleTkeo
		* -- varying_minRaw
		* -- varying_minTkeo

#### chan_in_dict : 

Donne un dictionnaire correspandant au canaux et avec les positions, avec raw.info["ch_names"] donne une liste avec le nom des canaux. 

#### get_list_chan : 

En récupérant le dictionnaire des canaux, on peut récupérer une liste des canaux. Utilisable dans la fonction get_manu.

* Keywords 

		* -- name

#### open_file_manu :

Ouverture des fichiers manuels en récupérant les colSample, colCode et colChan.

Charge les fichiers qui sont sous forme de signal continue, et segmente le signal avec le tmin et tmax défini.

* Keywords 

		* -- sj
		* -- colSample (défaut = 0)
		* -- colCode (défaut = 2) 
		* -- colChan (défaut = 3)
		* -- sf
		* -- manuEpochs

#### get_manu : 

Récupère le contenu du fichier provenant de manuEpochs et récupère le contenue sous forme de dictionnaire de listes d'objet Events avec les channel (canaux associés, on peut utiliser get_list_chan et chan_in_dict) et retourne ce dictionnaire en question. Supprimant les events après la fin.

* keywords

		* -- manu
		* -- dict_onsets

#### count_true_or_false_neg_pos :

A partir de la détection automatique (detect_one_trial_and_chan) un à un avec un event associé à onsetManu provenant du dictionnaire (get_manu), cela donnera une comparaison une à une via la fonction principal (detection_and_compute_f_score), pour retourner le nombre de FP, FN et de TP. 

Avant de faire la comparaison, on fait une copie d'onsetManu sinon, on perds des événements étant donné que l'on supprime chaque événement une fois trouvé, problèmatique dans le simplexe ou l'évolution différentielle.

* Keywords
 
		* -- onsetManu
		* -- onsetAuto
		* -- FP
		* -- FN
		* -- TP

## Description précise du optimisation_script :

Objet permettant principalement de faire tourner une méthode permettant d'essayer de trouver les paramètres optimisés suivant pour l'instant thRaw, thTk.

### Description de chaque méthode :

#### __init__ :

self.tmin, self.tmax, self.sujet, self.thRaw, self.thTk, self.time_limitRaw, self.time_limitTkeo, self.min_samplesRaw, self.min_sampleesTkeo, self.varying_minRaw, self.varying_minTkeo : paramètre sur lequel on peut jouer sauf self.sujet qui permet d'utiliser le numéro de fichier associé au participant étudié.

self.trials : permet de faire un tirage aléatoire des essais dans le fichier brut
self.EpEvts_random : permet de faire le test de l'optimisation sur les essais de taille différentes à partir d'un tirage aléatoire fait dans self.trials
self.dataEpochs_random : même chose que pour self.EpEvts_random

self.EpEvts_random et self.dataEpochs_random sont utilisées dans la méthode only_f_score en remplacer self.data[0] et self.data[1] par self.EpEvts_random et self.dataEpochs_random.

self.manu : ouverture du fichier une seule fois.

self.get_dict_manu : récupère le contenu de self.manu , dans un dictionnaire de liste d'objet de type events via l'appel de fonction get_manu de detectEMG_compute_f_score.py

self.matrix : crée une matrice de taille dépendant de thRaw et thTk 


#### __getitem__ :

retourne un index spécifique de la matrice

#### __setitem__ :

permet de mettre dans la matrice une valeur (value) à l'aide des index défini par index_x et index_y.

#### affiche_matrice :

affiche la matrice qui a été crée ou completé par le calcul du f1 score.

#### compute_f_score :

Réalise le calcul du f1 score uniquement et retourne le f1 score.

#### matrix_computation : 

Mets dans la matrice, les éléments d'une range dépendant de la valeur maximal de thTk et thRaw et fait la détection par différents seuil et mets le f score dans la matrice. 
[3] et [5] pour tester uniquement avec les seuils 3 et 5 de thRaw et thTK 

#### pivot_x et pivot_y :

récupère les éléments présents sur l'axe des x correspondant au thRaw pour le pivot_x et pour le pivot_y, on récupère l'axe des y de la matrice correspondant à thTk.

#### search_high_value :

Méthode qui va chercher la meilleur valeur et retourne la meilleur valeur présente de la matrice.

#### only_f_score :

Va écrire dans un fichier tous les scores calculer avec les différents paramètres lors de l'optimisation

thRaw = x[0] et thTk = x[1] permet de récupèrer les indices présent dans x0 pour le simplex ou les bounds. 

et fait la détection suivi du calcul.

#### try_f_optimize :

essai d'optimiser, le f score à partir de optimize.differential_evolution (enlever de commentaire les lignes de code pour faire optimize.minimize (simplexe)) et ouvre le fichier localisé dans le path d'intérêt, ne pas enlever le {self.sujet} cela vous permettra d'avoir le numéro du sujet associés dans la nom du fichier directement, vous pouvez cependant changer le path et la localisation des fichiers qui seront crée, voir même mettre en commentaire les lignes écrivant dans un fichier le résultat d'optimisation.



 
