import matplotlib.pyplot as plt
import re
import numpy as np
import pandas as pd
import glob
import seaborn as sns
plt.style.use("ggplot")

#Script qui me permet d'avoir les graphiques pour mon rapport.
def open_dataframe_sample_100(sj):
    global end_frame_sample100
    allfiles = glob.glob(f"run_script_sample/sujet_{sj}_*_*100.txt")
    max_idx = []
    df = []
    df_end = []
    count = 0
    for e in allfiles:
        data = pd.read_csv(e, sep = ",", names = ['thRaw', 'thTk', 'Score', 'Départ', "Sujet", "idxIteration"])
        data['Sujet'] = sj
        # data['Départ'] = e.replace("sample500.txt","").replace(f"run_script_sample/sujet_{sj}_", "")
        data['Départ'] = e.replace("_sample100.txt","").replace(f"run_script_sample/sujet_{sj}_", "")
        data['idxIteration'] = data.index + 1
        df_end += [data.iloc[-1, :]]



        df += [data]
    end_frame = pd.DataFrame(df_end)
    final_df = pd.concat(df, ignore_index = True)
    final_df['thRaw'] = final_df['thRaw'].map(lambda x: x.lstrip('thRaw : ')).astype(float)
    final_df['thTk'] = final_df['thTk'].map(lambda x: x.lstrip('thTk : ')).astype(float)
    final_df['Score'] = final_df['Score'].map(lambda x: x.lstrip("Score : ")).astype(float)
    final_df['Score'] = final_df['Score'] * (-1)
    # final_df['Départ'] = final_df['Départ'].astype(str)

    end_frame['thRaw'] = end_frame['thRaw'].map(lambda x: x.lstrip('thRaw : ')).astype(float)
    end_frame['thTk'] = end_frame['thTk'].map(lambda x: x.lstrip('thTk : ')).astype(float)
    end_frame['Score'] = end_frame['Score'].map(lambda x: x.lstrip("Score : ")).astype(float)
    end_frame['Score'] = end_frame['Score'] * (-1)


    return final_df

# def plot_try_read(sj):
#     lines = ""
#     for i in [5,10,15]:
#         for j in [7,14,21]:
#             file = open(f"run_script/sujet_{sj}_{i}_{j}.txt", "r")
#
#             lines += file.read()
#
#     pattern = re.compile(r"-[0-9]*.[0-9]*")
#
#     list = pattern.findall(lines)
#     list = np.asarray(list)
#     float_list = list.astype(np.float)
#     # print(list)
#     fig = plt.figure()
#     fig = plt.axes()
#     fig.plot(list, "ro")
#     fig.set_ylabel("Score")
#
#     max_value = max(float_list)
#     min_value = min(float_list)
#
#     number_of_step = 10
#     plt.yticks(np.arange(max_value, min_value, 0.5))
#     # l = np.arange(min_value, max_value, number_of_step)
#     #
#     #
#     # plt.yticks(np.arange(min_value, max_value))
#
#     plt.show()


def open_dataframe_sample_500(sj):
    global end_frame_sample500
    allfiles = glob.glob(f"run_script_sample/sujet_{sj}_*_*500.txt")
    max_idx = []
    df = []
    df_end = []
    count = 0
    for e in allfiles:
        data = pd.read_csv(e, sep = ",", names = ['thRaw', 'thTk', 'Score', 'Départ', "Sujet", "idxIteration"])
        data['Sujet'] = sj
        # data['Départ'] = e.replace("sample500.txt","").replace(f"run_script_sample/sujet_{sj}_", "")
        data['Départ'] = e.replace("_sample500.txt","").replace(f"run_script_sample/sujet_{sj}_", "")
        data['idxIteration'] = data.index + 1
        df_end += [data.iloc[-1, :]]



        df += [data]
    end_frame_sample500 = pd.DataFrame(df_end)
    final_df = pd.concat(df, ignore_index = True)
    final_df['thRaw'] = final_df['thRaw'].map(lambda x: x.lstrip('thRaw : ')).astype(float)
    final_df['thTk'] = final_df['thTk'].map(lambda x: x.lstrip('thTk : ')).astype(float)
    final_df['Score'] = final_df['Score'].map(lambda x: x.lstrip("Score : ")).astype(float)
    final_df['Score'] = final_df['Score'] * (-1)
    # final_df['Départ'] = final_df['Départ'].astype(str)

    end_frame_sample500['thRaw'] = end_frame_sample500['thRaw'].map(lambda x: x.lstrip('thRaw : ')).astype(float)
    end_frame_sample500['thTk'] = end_frame_sample500['thTk'].map(lambda x: x.lstrip('thTk : ')).astype(float)
    end_frame_sample500['Score'] = end_frame_sample500['Score'].map(lambda x: x.lstrip("Score : ")).astype(float)
    end_frame_sample500['Score'] = end_frame_sample500['Score'] * (-1)


    return final_df


def open_dataframe(sj):
    global end_frame
    allfiles = glob.glob(f"run_script/sujet_{sj}_*_*.txt")
    max_idx = []
    df = []
    df_end = []
    count = 0
    for e in allfiles:
        data = pd.read_csv(e, sep = ",", names = ['thRaw', 'thTk', 'Score', 'Départ', "Sujet", "idxIteration"])
        data['Sujet'] = sj
        # data['Départ'] = e.replace("sample500.txt","").replace(f"run_script_sample/sujet_{sj}_", "")
        data['Départ'] = e.replace(".txt","").replace(f"run_script/sujet_{sj}_", "")
        data['idxIteration'] = data.index + 1
        df_end += [data.iloc[-1, :]]

        # print(df_end)

        df += [data]
    end_frame = pd.DataFrame(df_end)
    final_df = pd.concat(df, ignore_index = True)
    final_df['thRaw'] = final_df['thRaw'].map(lambda x: x.lstrip('thRaw : ')).astype(float)
    final_df['thTk'] = final_df['thTk'].map(lambda x: x.lstrip('thTk : ')).astype(float)
    final_df['Score'] = final_df['Score'].map(lambda x: x.lstrip("Score : ")).astype(float)
    final_df['Score'] = final_df['Score'] * (-1)
    # final_df['Départ'] = final_df['Départ'].astype(str)

    end_frame['thRaw'] = end_frame['thRaw'].map(lambda x: x.lstrip('thRaw : ')).astype(float)
    end_frame['thTk'] = end_frame['thTk'].map(lambda x: x.lstrip('thTk : ')).astype(float)
    end_frame['Score'] = end_frame['Score'].map(lambda x: x.lstrip("Score : ")).astype(float)
    end_frame['Score'] = end_frame['Score'] * (-1)


    return final_df


def open_dataframe_de(sj):
    global end_frame_de
    allfiles = glob.glob(f"run_script_de/sujet_{sj}_DE_*_*.txt")
    max_idx = []
    df = []
    df_end = []
    count = 0
    for e in allfiles:
        data = pd.read_csv(e, sep = ",", names = ['thRaw', 'thTk', 'Score', 'Essai', "Sujet", "idxIteration"])
        data['Sujet'] = sj
        # data['Départ'] = e.replace("sample500.txt","").replace(f"run_script_sample/sujet_{sj}_", "")

        data['Essai'] = e.replace('_sampletest0.txt',"").replace(f"run_script_de/sujet_{sj}_", "").replace('_sampletest1.txt', "").replace('_sampletest2.txt',"").replace('_sampletest3.txt', "").replace('_sampletest4.txt', "")
        data['idxIteration'] = data.index + 1
        df_end += [data.iloc[-1, :]]



        df += [data]
    end_frame = pd.DataFrame(df_end)
    final_df = pd.concat(df, ignore_index = True)
    final_df['thRaw'] = final_df['thRaw'].map(lambda x: x.lstrip('thRaw : ')).astype(float)
    final_df['thTk'] = final_df['thTk'].map(lambda x: x.lstrip('thTk : ')).astype(float)
    final_df['Score'] = final_df['Score'].map(lambda x: x.lstrip("Score : ")).astype(float)
    final_df['Score'] = final_df['Score'] * (-1)
    # final_df['Départ'] = final_df['Départ'].astype(str)

    end_frame['thRaw'] = end_frame['thRaw'].map(lambda x: x.lstrip('thRaw : ')).astype(float)
    end_frame['thTk'] = end_frame['thTk'].map(lambda x: x.lstrip('thTk : ')).astype(float)
    end_frame['Score'] = end_frame['Score'].map(lambda x: x.lstrip("Score : ")).astype(float)
    end_frame['Score'] = end_frame['Score'] * (-1)


    return final_df

de_9 = open_dataframe_de(9)

# print(de_9)
df9 = open_dataframe(9)
df9_sample100 = open_dataframe_sample_100(9)
df9_sample500 = open_dataframe_sample_500(9)

# first_iteration = df9[df9['idxIteration'] == 1]
# start_end_iteration = first_iteration.append(end_frame, ignore_index = True)
#
# first_iteration_sample500 = df9_sample500[df9_sample500['idxIteration'] == 1]
# start_end_iteration_sample500 = first_iteration.append(end_frame_sample500, ignore_index = True)

df5 = open_dataframe(5)
df5_sample100 = open_dataframe_sample_100(5)
df5_sample500 = open_dataframe_sample_500(5)


# first_iteration = df5[df5['idxIteration'] == 1]
# start_end_iteration = first_iteration.append(end_frame, ignore_index = True)


df2 = open_dataframe(2)
df2_sample100 = open_dataframe_sample_100(2)
df2_sample500 = open_dataframe_sample_500(2)

#
first_iteration = df2[df2['idxIteration'] == 1]
start_end_iteration = first_iteration.append(end_frame, ignore_index = True)




# print(start_end_iteration)
# print(df[df[['Départ','idxIteration']] == np.max(df['idxIteration'])])

# print(df[df['Départ'].where(df['idxIteration'] == [df['idxIteration'].max()])])


# print(df9)
# print(df5)
# print(df2)


# sns.lmplot(x = 'idxIteration', y ='Score', data = df_end, hue = "Départ", palette = sns.color_palette("Set1", 9), fit_reg = False)
###############################################################################################################################################################
fig = plt.subplots(3, figsize = (10,7))
ax1 = plt.subplot(311)

# ax1.text(x= 0.5, y = 1.2, s= "Représentation des itérations des différentes combinaisons de points de départ (thRaw_thTk) en fonction du F1 score", fontsize = 13, weight='bold', ha='center', va='bottom', transform=ax1_2.transAxes)
sns.lineplot(x = 'idxIteration', y ='Score', data = df9, hue = "Départ", palette = sns.color_palette("Set1", 9), sort = False, ax = ax1, legend = False)

ax1.set_xlabel('')
ax1.set_xlim(0.90, 60)
ax1.tick_params(axis=u'both', which=u'both',length=0)

ax1.set(xticklabels = [])
plt.title("Participant 9", size = 10)

ax2 = plt.subplot(312)
sns.lineplot(x = 'idxIteration', y ='Score', data = df9_sample100, hue = "Départ", palette = sns.color_palette("Set1", 9), sort = False, ax = ax2, legend = "full")
ax2.set_xlabel('')
ax2.set_xlim(0.90, 60)
ax2.set(xticklabels = [])
ax2.tick_params(axis=u'both', which=u'both',length=0)
ax2.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.2, frameon = False)
plt.title("Participant 9 : tirage sample 100", size = 10)

ax3 = plt.subplot(313)
ax3.set_xlim(0.90, 60)
sns.lineplot(x = 'idxIteration', y ='Score', data = df9_sample500, hue = "Départ", palette = sns.color_palette("Set1", 9), sort = False, ax = ax3, legend = False)
plt.title("Participant 9 : tirage sample 500", size = 10)
#
#
#
#
#
#
#
# fig2 = plt.subplots(3)
# ax1_5 = plt.subplot(311)
# sns.lineplot(x = 'idxIteration', y ='Score', data = df5, hue = "Départ", palette = sns.color_palette("Set1", 9), sort = False, ax = ax1_5)
# ax1_5.set_xlabel('')
# plt.title("Sujet 5", size = 10)
# ax2_5 = plt.subplot(312)
# sns.lineplot(x = 'idxIteration', y ='Score', data = df5_sample100, hue = "Départ", palette = sns.color_palette("Set1", 9), sort = False, ax = ax2_5)
# ax2_5.set_xlabel('')
# plt.title("Sujet 5 : tirage sample 100", size = 10)
#
# ax3_5 = plt.subplot(313)
# sns.lineplot(x = 'idxIteration', y ='Score', data = df5_sample500, hue = "Départ", palette = sns.color_palette("Set1", 9), sort = False, ax = ax3_5)
# plt.title("Sujet 5 : tirage sample 500", size = 10)
#



#
# fig3 = plt.subplots(3, figsize = (10,7))
# ax1_2 = plt.subplot(311)
#
# # ax1_2.text(x= 0.5, y = 1.2, s= "Représentation des itérations des différentes combinaisons de points de départ (thRaw_thTk) en fonction du F1 score", fontsize = 13, weight='bold', ha='center', va='bottom', transform=ax1_2.transAxes)
# sns.lineplot(x = 'idxIteration', y ='Score', data = df2, hue = "Départ", palette = sns.color_palette("Set1", 9), sort = False, ax = ax1_2, legend = False)
#
# ax1_2.set_xlabel('')
# ax1_2.set_xlim(0.90, 60)
# ax1_2.tick_params(axis=u'both', which=u'both',length=0)
#
# ax1_2.set(xticklabels = [])
# plt.title("Participant 2", size = 10)
#
# ax2_2 = plt.subplot(312)
# sns.lineplot(x = 'idxIteration', y ='Score', data = df2_sample100, hue = "Départ", palette = sns.color_palette("Set1", 9), sort = False, ax = ax2_2, legend = "full")
# ax2_2.set_xlabel('')
# ax2_2.set_xlim(0.90, 60)
# ax2_2.set(xticklabels = [])
# ax2_2.tick_params(axis=u'both', which=u'both',length=0)
# l = ax2_2.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.2, frameon = False)
#
# plt.title("Participant 2 : tirage sample 100", size = 10)
#
# ax3_2 = plt.subplot(313)
# ax3_2.set_xlim(0.90, 60)
# sns.lineplot(x = 'idxIteration', y ='Score', data = df2_sample500, hue = "Départ", palette = sns.color_palette("Set1", 9), sort = False, ax = ax3_2, legend = False)
# plt.title("Participant 2 : tirage sample 500", size = 10)
####################################################################################################################################################################


# fig4 = plt.subplot()
# ax1 = plt.subplot(111)
# sns.(x = "thRaw", y = "thTk", data = df9)
# sns.kdeplot(df9['thTk'], ax = ax1)
# ax2 = plt.subplot(212)
# ax3 = plt.subplot(321)



# print(df9)
# print(start_end_iteration)
# fig = plt.subplots(1, figsize = (7,7))
# ax_chemin = plt.subplot(111)
# sns.lineplot(x = "thRaw", y = "thTk",data = df2, hue = "Départ", sort = False, estimator = None, palette = sns.color_palette("Set1", 9), ax = ax_chemin)
# ax_chemin.set_xlabel('')
# ax_chemin.set_xlim(4, 20)
# ax_chemin.legend(loc = 'lower right', frameon = False)
# sns.scatterplot(x = "thRaw", y = "thTk", data = start_end_iteration, hue = "Départ" ,palette = sns.color_palette("Set1", 9), ax = ax_chemin, legend = False, s = 50)

# plt.title("Représentation des différents chemins de seuils suivi par rapport au combinaison de point de départ")
# sns.lineplot(x = "thRaw", y = "thTk",data = df9_sample500, hue = "Départ", sort = False, estimator = None,  palette = sns.color_palette("Set1", 9))


# sns.scatterplot(x = "thRaw", y = "thTk", data = start_end_iteration_sample500, hue = "Départ")
#,  palette = sns.color_palette("Set1", 9)





# sns.lineplot(x = "thRaw", y = "thTk",data = df5, hue = "Départ", sort = False, estimator = None, palette = sns.color_palette("Set1",9))
#
#
# sns.scatterplot(x = "thRaw", y = "thTk", data = start_end_iteration, hue = "Départ", palette = sns.color_palette("Set1", 9))






# sns.lineplot(x = "thRaw", y = "thTk",data = df2, hue = "Départ", sort = False, estimator = None, palette = sns.color_palette("Set1", 9))
#
#
# sns.scatterplot(x = "thRaw", y = "thTk", data = start_end_iteration, hue = "Départ", palette = sns.color_palette("Set1", 9))

# plt.savefig("Sujet_2_idxIteration_Fscore.pdf")
# ax_de1, ax_de2 = plt.subplots()





# fig1 = plt.subplots()
# ax_de1 = plt.subplot(111)
# sns.scatterplot(x = "thRaw", y = "thTk", data = de_9, hue = "Essai", ax= ax_de1, marker = "D", style = "Essai", s =100, hue_order = ["DE_2500","DE_1000", "DE_500", "DE_100"])
# # plt.title("Observation de la répartition des Seuils")
# ax_de1.legend(markerscale = 2)


#
# fig2= plt.subplots()
# ax_de2= plt.subplot(111)
# sns.catplot(x= "Essai", y= "Score", data = de_9, ax=  ax_de2)
# ax_de2.set_ylim(0.930, 1)


# plt.savefig("Sujet_9_chemin_seuil_opg_test.pdf")
# plt.savefig("Sujet_9_DE_rapport_repartition_seuil.pdf")
# plt.savefig("Simplexe_figure2_sujet2_different_tirage_powerpoint.pdf")
# plt.savefig("Participant_9_rapport.pdf")
# plt.savefig("DE9_order.pdf")
plt.show()
