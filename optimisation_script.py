#!/usr/bin/env python
"""algo simplex"""
import detectEMG_compute_f_score as detect_EMG
import numpy as np
#test.f_score(10,10,2)
import random
import pandas as pd
from multiprocessing import Pool

import sys
import os
sys.path.append(os.path.abspath("/home/calcul/trunk"))


from Events import Events, Latency
from scipy import optimize


#We gonna use simplex of scipy called Nelder-Meed
class optimize_param():

    """

    Tables of different f_score obtain with parameter like thRaw and thTk
    thRaw -- Raw threshold
    thTk  -- Threshold with Teager-Kaiser method
    sujet -- participant files numbers
    And this object select the best value with the best F_score in matrix computation method

    try_f_optimize method use the method only_f_score method get the f_score with the calling function of detection_and_compute_f_score and this method is used on
    (simplex algorithm) optimize.minimize with the method Nelded-mead and you can define the maxiteration

    """

    def __init__(self, sujet, thRaw = 3, thTk = 5,\
    time_limitRaw = 0.025, time_limitTkeo = 0.025, min_samplesRaw = 3, min_samplesTkeo = 10, varying_minRaw = 1, varying_minTkeo = 0, tmin = (-0.3), tmax = 1.5):

        """

        Construction de l'objet avec les paramètres suivants

        Keywords Arguments important :
        ------------------------------

        sujet -- numéro du participant correspondant au fichier brut étudié
        thRaw -- Seuil brut (par défaut = 3)
        thTk  -- Seuil utilisant la méthode de Teager Kaiser (par défaut = 5)
        tmin  -- temps correspondant au départ du signal
        tmax  -- temps correspond à la fin du signal

        Autre paramètres :
        ------------------
        time_limitRaw (par défaut = 0.025)
        time_limitThTK (par défaut = 0.025)
        min_samplesRaw (par défaut = 3)
        min_samplesTkeo (par défaut = 10)
        varying_minRaw (par défaut = 1)
        varying_minTkeo (par défaut = 0)

        Constructeur permettant de charger le fichier brut une seul fois, mais aussi de charger le fichier manuel et d'obtenir le dictionnaire de liste d'objet Events

        self.trial / self.EpEvts_random / self.dataEpochs_random: permet de faire un test sur les algorithmes de simplexe et d'évolution différentiel à partir d'un tirage aléatoire dans les fichiers de taille différentes
        taille à changer dans self.trials (size = 100)

        """
        self.tmin = tmin
        self.tmax = tmax
        self.sujet = sujet
        self.thRaw = thRaw
        self.thTk = thTk
        self.time_limitRaw = time_limitRaw
        self.time_limitTkeo = time_limitTkeo
        self.min_samplesRaw = min_samplesRaw
        self.min_samplesTkeo = min_samplesTkeo
        self.varying_minRaw = varying_minRaw
        self.varying_minTkeo = varying_minTkeo

        self.data = detect_EMG.load_data(self.sujet, self.tmin, self.tmax) #Ouverture du fichier raw (1 fois)

        self.trials = np.random.randint(0, self.data[0].nbTrials(), size = 100)
        # self.trials= np.arange(0,100) # tirage des 100 premiers essais

        self.EpEvts_random = self.data[0].getTrials(self.trials)
        self.dataEpochs_random = self.data[1][self.trials]

        self.manu = detect_EMG.open_file_manu(self.tmin, self.tmax, self.sujet) #Ouverture du fichier manuelle (1 fois)

        # self.manu = self.manu.getTrials(self.trials)

        self.get_dict_manu = detect_EMG.get_manu(self.manu) #récupère un dictionnaire avec les chans et les objets Events





        self.matrix = np.zeros([thRaw - 1, thTk - 3], dtype = float) #Essai d'obtenir la première ligne et column avec les indices correspondant au seuil

        self.dictionnaire_values = {}
        self.value_x_conserv = []
        self.value_y_conserv = []

    def __getitem__(self, index):

        """

        Get item of matrix in specific index

        """

        return self.matrix[index]

    def __setitem__(self, index_x, index_y, value):

        """

        Set value in the matrix by using the x and y

        """

        self.matrix[index_x][index_y] = value


    def affiche_matrice(self):

        """

        Affiche la matrice pour les différents calculs appliqués à partir de la méthode matrix_computation

        """

        print(self.matrix)



    def compute_f_score(self, thRaw, thTk): # , thRaw, thTk, time_limitRaw = 0.025, time_limitTkeo = 0.025, min_samplesRaw = 3, min_samplesTkeo = 10, varying_minRaw = 1, varying_minTkeo = 0

        """

        Réalise la détection uniquement et le calcul du f_score

        """

        self.result_detection = detect_EMG.detection_and_compute_f_score(thRaw, thTk, self.EpEvts_random, self.dataEpochs_random, self.data[2], self.data[3], self.get_dict_manu)
        # self.result_detection = detect_EMG.detection_and_compute_f_score(thRaw, thTk, self.data[0], self.data[1], self.data[2], self.data[3], self.get_dict_manu,\
        # self.time_limitRaw, self.time_limitTkeo, self.min_samplesRaw, self.min_samplesTkeo, self.varying_minRaw, self.varying_minTkeo)

        return self.result_detection

    def matrix_computation(self):

        """

        compute f_score in matrix

        """

        for i in [3]:#range(3, self.thRaw + 1): #+1 pour avoir le dernier seuil qui sera manquant
            self.matrix[0][i - 2] = int(i) #mets les différents seuil de thRaw -2 pour être dans la position [1][0] sur la matrice sachant le départ 3
            for j in [5]:
                self.matrix[j - 4][i - 2] = detect_EMG.detection_and_compute_f_score(i, j, self.data[0], self.data[1], self.data[2], self.data[3], self.get_dict_manu,\
                self.time_limitRaw, self.time_limitTkeo, self.min_samplesRaw, self.min_samplesTkeo, self.varying_minRaw, self.varying_minTkeo)#calcul du f1_score
                self.matrix[j - 4][0] = int(j)
                self.matrix[0][0] = None

        return self.matrix

    def pivot_x(self):

        """

        Getting the X row of the first row with the thRaw

        """

        for i in range(0, len(self.matrix)): #Pivot donnant les seuils de thtk
            self.value_x_conserv += [self.matrix[0][i]] #change les lignes

        return self.value_x_conserv

    def pivot_y(self):

        """

        Getting the Y first column with the thTk threshold value

        """

        #pivot donnant les seuils de thRaw
        for j in range(0, len(self.matrix)):
            self.value_y_conserv += [self.matrix[j][0]] #change les colonnes

        return self.value_y_conserv

    def search_high_value(self):

        """

        Search the highest score in the matrix and compare the highest value with another value in the matrix
        and change the highest value if scores > max_s and replace max_s with the max(scores) value

        """

        pivot_x_thraw = self.pivot_x() #getting the x value on the matrix
        pivot_y_thtk = self.pivot_y() #getting the y value on the matrix

        index_i = 0
        index_j = 0
        max_s = 0

        for l in range(1, len(self.matrix)):
            row = self.matrix[l]
            scores = row[1 : len(self.matrix)]
            if max(scores) > max_s:
                max_s = max(scores) #prend le meilleur score présent
                index_i = l
                index_j = np.min(np.where(row == max_s)) #search the best value
        #print("i, j = %i , %i" %(index_i, index_j))
        best_thtk = pivot_x_thraw[index_j]
        best_thraw = pivot_y_thtk[index_i]

        return "\n Seuils les plus optimaux ThTk = " + str(best_thtk) + " et thRaw = " + str(best_thraw) + " pour avoir le meilleur F score."

    def only_f_score(self, X):

        """

        Réalise la détection uniquement

        """

        thRaw = X[0]
        thTk = X[1]
        # result_detection = detect_EMG.detection_and_compute_f_score(thRaw, thTk, self.EpEvts_random, self.dataEpochs_random, self.data[2], self.data[3], self.get_dict_manu) #tirage aléatoire
        result_detection = detect_EMG.detection_and_compute_f_score(thRaw, thTk, self.data[0], self.data[1], self.data[2], self.data[3], self.get_dict_manu) #Changer le self.data[0] par self.EpEvts_random et self.data[1] par dataEpochs_random pour faire l'analyse avec tirage aléatoire


        file_write.writelines(f"thRaw : {thRaw}, thTk : {thTk}, Score : {result_detection} \n")
        return result_detection

    def try_f_optimize(self): #maxiter

        """

        Minimisation en utilisant la méthode Nelder-Mead dans l'algorithme de simplexe utilisé avec optimize.minimize contenue dans Scipy (en commentaire)
        Utilise actuellement l'approche algorithmique d'évolution différentielle avec optimize.differential_eviolution contenue dans Scipy.

        """
        global file_write


        #x0 = [15, 15]
        # for i in range(5, 16, 5):
        #     for j in range(7, 22, 7):
        #         #file_write = open(f"run_script/sujet_{self.sujet}_{i}_{j}.txt", "w") #modify the path
        #         file_write = open(f"run_script_sample/sujet_{self.sujet}_{i}_{j}_sample100.txt", "w")
        #         x0 = [i,j]
        #         optimize.minimize(self.only_f_score, x0, method = "Nelder-Mead", options= {'maxiter' : maxiter, 'adaptive' : True})

        i = 0
        while i < 5: #fait 5 fois l'optimisation par differential evolution
            print(i)
            file_write = open(f"run_script/sujet_{self.sujet}_DE_2500_sampletest{i}.txt", "w") #modify the path
            print(optimize.differential_evolution(self.only_f_score, bounds = [(5,20), (7,21)], popsize = 15, strategy = 'randtobest1bin'))

            print("randtobest1bin 2500")
            i += 1



if __name__ == "__main__":

    # a = simplex_algo_but_incomplet(3, 5, 1) #simplex_algo(thRaw, thTk, data) # tester avec 3 / 5
    # a = simplex_get_optimized_param(1)
    # a.compute_f_score(3,5)
    a = optimize_param(9)
    a.try_f_optimize()
