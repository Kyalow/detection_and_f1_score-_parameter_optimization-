# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 17:30:02 2018

@author: Laure
"""
import sys
import os
sys.path.append(os.path.abspath("/home/calcul/trunk"))

import numpy as np
import mne
import emgTools as emg
import useMne
from Events import Events, Latency
import evtTools as evt
import utils
import pandas as pd
import itertools as it


user = "laure"

if user=="laure":
    pathBDF = os.path.join('/home','calcul','Documents','Yousri','DataBDF')
    pathauto = os.path.join('/home','calcul','Documents','Yousri','automatic_detection')
    pathManu = os.path.join("/home","calcul" ,"Documents", "Yousri", "manual_events_byLaure_inContinuous")



#user = "laure"

#if user=="laure":
#    pathBDF = os.path.join('D:/','LNC','Gabriel','PhD','DataBDF')
#    pathauto = os.path.join('D:/','LNC','Gabriel','PhD','automatic_detection', 'november_2018')


trig_id = {'SpeedLeft1':111,'SpeedLeft2':112,'SpeedLeft3':113,\
           'SpeedRight1':121,'SpeedRight2':122,'SpeedRight3':123,\
           'AccLeft1':211,'AccLeft2':212,'AccLeft3':213,\
           'AccRight1':221,'AccRight2':222,'AccRight3':223} #FIXME update with correct triggers values

#to map the correct type in BVA to each marker. FIXME: Update with your values
eventType={111:'Stimulus',112:'Stimulus',113:'Stimulus',121:'Stimulus',122:'Stimulus',123:'Stimulus',\
           211:'Stimulus',212:'Stimulus',213:'Stimulus',221:'Stimulus',222:'Stimulus',223:'Stimulus',\
           100:'Response',200:'Response',101:'EMG',102:'EMG',202:'EMG',204:'EMG'}

# lists response triggers (used if we want to ignore EMG bursts after response)
resp_id = [ e[0] for e in eventType.items() if e[1] == 'Response']

emg_id = {'onset':4 , 'offset' : 5}

# add dictionnary of response id
Resp_side_id = {100: 0, 200: 1}
#add dictionnary correct resp id
correct_resp_id = {111:100, 112:100, 113:100, 121:200, 122:200, 123:200,\
                   211:100, 212:100, 213:100, 221:200, 222:200, 223:200}

mBl = []
stBl =[]



#list_dir = ['S2.bdf']    # to process only one file. recommended...
list_dir = os.listdir(pathBDF)
useMbl = None
useMblStd = None

# load raw data
def load_data(sujet, tmin, tmax):

    """

    Chargement des données brutes

    Keyword Arguments :
    ------------------

    sujet       -- correspond au numéro du fichier dépendant du sujet
    tmin        -- temps minimal lié au départ d'un échantillon de signal
    tmax        -- temps maximal lié à la fin du signal

    Global variable :
    -----------------
    raw         -- correspond à la variable qui ouvre le fichier brute
    lat_lag     -- float : time of latency lag

    """

    global raw, lat_lag

    sj = sujet
    lat_lag = 0.05

    nameBDF = 'S' + str(sj) + '.bdf'
    fname = os.path.join(pathBDF,nameBDF)

    raw = mne.io.read_raw_edf(fname,montage=None, preload=True,stim_channel = 'Status')



    if sj == 1:
        mne.set_bipolar_reference(raw,anode=['EXG1-0','EXG3-0'],\
        cathode=['EXG2-0','EXG4-0'],ch_name=['EMG_L','EMG_R'],\
        copy=False)
    else:
        mne.set_bipolar_reference(raw,anode=['EXG1','EXG3'],\
        cathode=['EXG2','EXG4'],ch_name=['EMG_L','EMG_R'],\
        copy=False)
    events = mne.find_events(raw, stim_channel = 'STI 014',shortest_event=1)
    events[:,2] = events[:,2] - events[:,1]
    events[:,1] = events[:,1] - events[:,1]

    raw.pick_channels(['EMG_L','EMG_R','Erg1','Erg2'])
    raw.set_channel_types({'EMG_L':'emg',
                           'EMG_R':'emg',
                           'Erg1':'misc',
                           'Erg2':'misc'})

    # filter EMG
    raw = useMne.applyFilter(raw, ch_names = ['EMG_L','EMG_R'], low_cutoff = 10)

    Evts = Events(sample = events[:,0], code = events[:,2], chan = [-1] * events.shape[0], sf = raw.info['sfreq'])
    EpEvts = Evts.segment(codeT0 = list(trig_id.values()), tmin = tmin, tmax = tmax)
    dataEpochs = evt.getDataEpochs(raw._data, EpEvts)
    epochTime = evt.times(tmin, tmax, raw.info['sfreq'])

    t0 = emg.findTimes(0,epochTime)

    return EpEvts, dataEpochs, epochTime, t0

def detect_one_trial_and_chan(c, thRaw, thTk, EpEvts, current, epochTime, t0, time_limitRaw = 0.025, time_limitTkeo = 0.025, min_samplesRaw = 3, min_samplesTkeo = 10, varying_minRaw = 1, varying_minTkeo = 0):

    """

    Permit to detect one trial with one chan and it's used in detection function

    Keywords arguments :
    --------------------
    c                   -- channel used in detection function with the loop (in range (2))
    current             -- link to dataEpochs and get the current trials for specific channel
    epochTime           -- vector of time based on sample
    t0                  -- find the t0 with the first value of epochTime

        Parameters arguments :
        ----------------------

        thRaw           -- raw threshold
        thTk            -- threshold use Teager Kaiser method
        time_limitRaw   -- float
        time_limitTkeo  -- float
        min_samplesRaw  -- float
        min_samplesTkeo -- float
        varying_minRaw  -- float
        varying_minTkeo -- float

    Return
    ----------------------------

    onsetsEvts and offsetsEvts

    """

    #récupères les onsets et offsets
    onsets, offsets = emg.getOnsets(current,epochTime,sf = raw.info['sfreq'],\
                                   thRaw = thRaw, useRaw = True, time_limitRaw = time_limitRaw, min_samplesRaw = min_samplesRaw, varying_minRaw = varying_minRaw, mBlRaw = None, stBlRaw = None, \
                                   thTkeo = thTk, useTkeo = True, time_limitTkeo = time_limitTkeo,  min_samplesTkeo = min_samplesTkeo, varying_minTkeo = varying_minTkeo, mBlTkeo = None, stBlTkeo = None)


    # Removes burst starting and ending before t0
    onsets = [onsets[b] for b in range(len(onsets)) if (offsets[b] > t0)]
    offsets = [offsets[b] for b in range(len(offsets)) if (offsets[b] > t0)]

    # If one onset remains before t0, put its latency to t0
    #onsets = [np.max((b,t0+1)) for b in onsets]

    # Removes bursts starting after the first response
    stim = EpEvts.findEvents(code = list(trig_id.values()))
    resp = EpEvts.findEvents(code = resp_id)


    #latency of the first response after the first stimulus
    if len(resp) > 0:
        resp_latency = EpEvts.Latency.sample[resp[resp > stim[0]][0]]

        offsets = [offsets[b] for b in range(len(offsets)) if (onsets[b] < resp_latency)]
        onsets = [onsets[b] for b in range(len(onsets)) if (onsets[b] < resp_latency)]

    onsetEvts = Events(sample = onsets, code = [emg_id['onset']] * len(onsets), chan = [c] * len(onsets), sf = raw.info['sfreq']) #mets les onsets dans un objet Event en récupérant le code associé au dictionnaire emg_id, * len(onsets) permet d'avoir la même taille
    offsetEvts = Events(sample = offsets, code = [emg_id['offset']] * len(offsets), chan = [c] * len(offsets), sf = raw.info['sfreq'])

    onsetEvts.Latency.time = onsetEvts.Latency.time + epochTime[0] #permet de mettre au même temps que les manuels
    offsetEvts.Latency.time = offsetEvts.Latency.time + epochTime[0]

    # epochTime[offsets]

    return onsetEvts, offsetEvts

def detection_and_compute_f_score(thRaw, thTk, epEvts, dataEpochs, epochTime, t0, manu, time_limitRaw = 0.025, time_limitTkeo = 0.025, min_samplesRaw = 3, min_samplesTkeo = 10, varying_minRaw = 1, varying_minTkeo = 0):

    """

    Permet de réaliser l'étape de détection et de calculer le f1_score avec la recherche de falsePositive, falseNegative, truePositive
    ---------------------------------------------


    Keywords arguments:
    --------------------

    epEvts      -- correspond à la segmentation de Evts avec tmin et tmax spécifié avant
    dataEpochs  -- correspond au epochEvents segmenté
    epochTime   -- Array correspondant à la conversion sample / temps
    manu        -- dictionnaire récupéré avec la fonction get_manu, récupère un dictionnaire {chan: [Events()]}

        Parametrès d'intérêts:
        ----------------------

        thRaw           -- raw threshold
        thTk            -- threshold use Teager Kayser method
        time_limitRaw   -- float
        time_limitTkeo  -- float
        min_samplesRaw  -- float
        min_samplesTkeo -- float
        varying_minRaw  -- float
        varying_minTkeo -- float

    Return:
    -------

    F_measure -- correspond au calcul donnant le F1_score

    """

    nb_TP = []
    nb_FN = []
    nb_FP = []

    for e in range(dataEpochs.shape[0]):

        for c in range(2):
            onset_detect_auto = detect_one_trial_and_chan(c, thRaw, thTk, epEvts.listEvtsTrials[e], dataEpochs[e,c,:], epochTime, t0, time_limitRaw, time_limitTkeo, min_samplesRaw, min_samplesTkeo, varying_minRaw, varying_minTkeo)[0]

            result = count_true_or_false_neg_pos(manu[c][e], onset_detect_auto)


            nb_TP += [result[2]]
            nb_FN += [result[1]]
            nb_FP += [result[0]]

    TP = sum(nb_TP)
    if TP == 0: # dans le cas très rare ou on aurait pas de Vrai positive, ce qui pourrait amener à une erreur dans le simplexe et donc à un arrêt prématuré du script
        TP = 1
    FN = sum(nb_FN)
    FP = sum(nb_FP)

    PPV = TP/ (TP + FP)
    sensitivity = TP / (TP + FN)
    F_measure = 2 * PPV * sensitivity / (PPV + sensitivity)
    invF_measure = F_measure * (-1)

    #print(invF_measure)
    return invF_measure



#récupération des chans dans un dictionnaire pour l'utiliser plus tard dans isolement chan
def chan_in_dict():

    """

    Get a dict of ch_names with number position

    """
    ch_names_info = raw.info["ch_names"]

    dict_names = {}
    for e in range(len(ch_names_info)):
        dict_names[ch_names_info[e]] = e
    return dict_names

def get_list_chan(name):

    """

    Get the dictionnary of chan_in_dict for return a list of chan

    Keyword argument :
    ------------------
        name -- names of chan present in ch_names raw.info["ch_names"]


    Return :
    --------
        Return position present in get_chan for specific mne.info["ch_names"] dictionnary

    """

    chan_dictionnary = chan_in_dict()
    liste = [chan_dictionnary[e] for e in name]

    return liste

def open_file_manu(tmin, tmax, sj, colSample = 0, colCode = 2, colChan = 3):

    """

    Ouverture et récupération des colonnes d'intérêts

    Keywords arguments:
    -------------------

    sj              -- sujet lié au numéro du sujet du fichier d'intérêt
    colSample = 0   -- colonne associé aux échantillons (default value)
    colCode = 2     -- colonne associé aux codes (default value)
    colChan = 3     -- colonne associé aux Channel (default value)


    Paramètres dans l'ouverture du fichier hors argument :
    ------------------------------------------------------

    sf              -- correspond à la fréquence d'échantillon ici raw.info["sfreq"] récupère la fréquence associé au fichier brut

    Return :
    --------

    manuEpochs      -- Segmentation du fichier après ouverture en fonction d'un tmin / tmax spécifié

    """

    manu = evt.loadContinuous(os.path.join(pathManu, "S" + str(sj) + ".csv"), colSample = colSample, colCode = colCode, colChan = colChan, sf = raw.info['sfreq']) #raw.info['sfreq'] récupèration de la fréquence associé au fichier brut

    manuEpochs = manu.segment(codeT0 = list(trig_id.values()), tmin = tmin, tmax = tmax)

    return manuEpochs

def get_manu(manu, channel = [0, 1]):

    """

    Get a dict with a list of object Events depending of channel

    Keywords Arguments :
    --------------------

    manu            --  Get the open_load_manu (return of manual file and segmented) (manuEpochs)

    Return :
    --------

    dict_onsets     --  keys/values of dict_onsets = {channel : [object]}

    """

    manuEvtsTrial = []
    trials = np.arange(manu.nbTrials())
    dict_onsets = dict()

    for ch in channel:
        dict_onsets[ch] = []

    for e in trials:
        endManu = evt.idxEndEvent(manu.listEvtsTrials[e].code, manu.listEvtsTrials[0].chan, manu.listEvtsTrials[0].Latency.time,\
                                  trig_id.values(), Resp_side_id.keys(), onsetCodes=4, offsetCodes=5) #récupère l'index du dernier event

        manu.listEvtsTrials[e].delEvents(np.arange(endManu+1,manu.listEvtsTrials[e].nbEvents()), printDelEvt=False)#suppression des Events après la fin

        for ch in channel:
            dict_onsets[ch].append(manu.listEvtsTrials[e].findAndGetEvents(code=4, chan=ch, printFindEvt=False))

    return dict_onsets


def count_true_or_false_neg_pos(onsetManu, onsetAuto):

    """

    Count the number of false positive and true positive and false negative for computation and getting f1_score

    Keywords arguments :
    --------------------

    onsetManu   -- need object of manuel Events onset used in detection function
    onsetAuto   -- need object of automatic Events onset used in detection function

    Return :
    --------

    FP          -- Return the false positive value
    FN          -- Return the false negative value
    TP          -- Return the true positive value

    """

    mb = 0 ; ab = 0

    FP = 0
    TP = 0
    FN = 0

    copy_Manu = onsetManu.copyEvents() #Copy nécessaire sinon on perd les events dans le simplex

    while (mb < copy_Manu.nbEvents()) & (ab < onsetAuto.nbEvents()): # Attention au parenthèse sinon fausse la comparaison

        if np.abs(onsetAuto.Latency.time[ab] - copy_Manu.Latency.time[mb]) < lat_lag:
            TP += 1
            copy_Manu.delEvents(mb, printDelEvt = False)#delete event
            onsetAuto.delEvents(ab, printDelEvt = False)

        elif ab == onsetAuto.nbEvents()-1:
            mb += 1
            ab = 0
        else:
            ab += 1

    FP = copy_Manu.nbEvents() #add nbre FP par rapport au event restant
    FN = onsetAuto.nbEvents()

    return FP, FN, TP


####################################################################################################################################################################################################################################
#                                                                                                                                                                                                                                  #
#                                                               ENTREZ LES PARAMETRES CI-DESSOUS                                                                                                                                   #
#                                                                                                                                                                                                                                  #
####################################################################################################################################################################################################################################

if __name__ == "__main__":
    tmin = -.3
    tmax = 1.5
    dataload = load_data(9, tmin, tmax)
    manu = open_file_manu(tmin, tmax, 9)
    dictionnaire_canaux = chan_in_dict()
    list_chan = get_list_chan(dictionnaire_canaux)
    get_manuel = get_manu(manu, list_chan)
    print(detection_and_compute_f_score(3,5, dataload[0], dataload[1], dataload[2], dataload[3], get_manuel))

####################################################################################################################################################################################################################################
