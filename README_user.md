# Manuel utilisateur:

## Tableau de contenu
* [Info detectEMG_compute_f_score.py](#détection-et-calcul-du-f1-score)
* [Info optimisation_script.py](#utilisation-de-optimisation_scriptpy)

## Détection et calcul du F1 score

Pour pouvoir utiliser ce script, il vous faut dans un premier temps fixer les dictionnaires suivant : trig_id, eventType, emg_id, correct_resp_id, resp_side_id.
Une fois fixé, vous pouvez lancer le script en réalisant la commande suivant : python detectEMG_compute_f_score.py ou python 
, dans la partie if __name__ == "__main__" et jouer sur les paramètres que vous désirez.

Les différents paramètres que vous pouvez régler sont les suivants:

* tmin, tmax

Et pour les différentes fonctions :

* load_data : sujet, tmin, tmax. cette fonction va charger le fichier brut | Attention il faut que le nom du fichier commence par "S(numéro sujet).bdf, tout en spécifiant le chemin du fichier au tout début du script dans pathBDF| 

* open_file_manu : tmin, tmax, sujet. Fonction qui va charger le fichier manuel | Nom de fichier doit être le suivant : "S(numéro sujet).csv tout en spécifiant le chemin où est localisé votre dossier contenant les fichiers traités manuellement dans pathManu au début du script |


* get_manu : manu (correspond au fichier qui a été chargé). Donne un dictionnaire associé à chaque événement par canaux.

* detection : thRaw, thTk, EpEvts, dataEpoch, epochTime, t0 paramètre obligatoire, après il y a des paramètres qui sont mis par défaut comme time_limitRaw (défaut = 0.025), time_limitTkeo (défaut = 0.025), min_samplesRaw (défaut = 3), min_samplesTkeo (défaut = 10), varying_minRaw (défaut = 1), varying_minTkeo (défaut = 0)


Ce script vous permettra d'obtenir directement le F1 score après l'étape de détection automatique, mais vous avez besoin d'utiliser des fichiers traités manuellement pour pouvoir lancer ce script sans problème.


## Utilisation de optimisation_script.py

Objet optimize_param():

Tout d'abord, l'initialisation de l'objet doit être fait dans une variable.
par exemple

variable = simplex_get_optimized_param(sujet, thRaw, thTk ...)

Plusieurs paramètres sont disponibles dans l'objet :

Tel que :

		* sujet
		* thRaw (Défaut = 3)
		* thTk	(Défaut = 5)
		* time_limitTkeo (Défaut = 0.025)
		* time_limitRaw (Défaut = 0.025)
		* min_samplesTkeo (Défaut = 3)
		* min_samplesRaw  (Défaut = 10)
		* varying_minRaw  (Défaut =  1)
		* varying_minTkeo (Défaut = 0)
		* tmin (Défaut = -0.3)
		* tmax (Défaut = 1.5)

Ensuite plusieur méthode sont à votre disposition :

variable.matrix_computation() == Va calculer différents F1 score et le mettre dans une matrice
variable.afficher_matrice() == Affiche la matrice après l'utilisation de la méthode matrix_computation()

only_f_score() == méthode qui va être utilisé dans la méthode try_f_optimize(), calcul le F score (Ne pas toucher à cette méthode)

try_f_optimize() == Utilise la méthode de simplexe (plûtot la méthode d'évolution différentiel), vous pouvez mettre le nom de votre path sans retirer la partie {self.sujet} qui mettra le numéro du sujet étudié.

compute_f_score() == Méthode qui va calculer directement le F score

Le reste des méthodes qui n'ont pas de description ne sont pas à utiliser.




